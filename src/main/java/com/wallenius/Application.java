package com.wallenius;

import com.wallenius.data.DocumentRepository;
import com.wallenius.data.FileStore;
import com.wallenius.data.InMemoryFileStore;
import com.wallenius.data.NaiveDocumentRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DocumentRepository documentRepository() {
        return new NaiveDocumentRepository();
    }
    
    @Bean
    public FileStore fileStore() {
        return new InMemoryFileStore();
    }

}
