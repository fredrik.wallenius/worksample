package com.wallenius.data;

import com.wallenius.data.holders.BinaryFile;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryFileStore implements FileStore {

    private final Map<String, BinaryFile> fileStore = new HashMap<>();
    
    @Override
    public void save(String fileName, BinaryFile binaryFile) {
        this.fileStore.put(fileName, binaryFile);
    }

    @Override
    public Optional<BinaryFile> get(String fileName) {
        BinaryFile file = this.fileStore.get(fileName);
        if (file == null) {
            return Optional.empty();
        } else {
            return Optional.of(file);
        }
    }
    
}
