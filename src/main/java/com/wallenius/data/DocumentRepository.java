package com.wallenius.data;

import com.wallenius.data.holders.BinaryFile;
import com.wallenius.data.holders.Document;
import java.util.List;
import java.util.Optional;

public interface DocumentRepository {

    void put(Document newDocument, BinaryFile binaryFile);
    
    Optional<BinaryFile> get(String fileName);

    List<Document> getAll();

    List<Document> getByCategory(String category);

}
