package com.wallenius.data;

import com.wallenius.data.holders.BinaryFile;
import java.util.Optional;

public interface FileStore {

    void save(String fileName, BinaryFile binaryFile);
    
    Optional<BinaryFile> get(String fileName);
    
}
