package com.wallenius.data.holders;

import java.util.List;

public class Document {

    private final String fileName;
    private final List<String> categories;

    public Document(String fileName, List<String> categories) {
        this.fileName = fileName;
        this.categories = categories;
    }

    public String getFileName() {
        return fileName;
    }

    public List<String> getCategories() {
        return categories;
    }

}
