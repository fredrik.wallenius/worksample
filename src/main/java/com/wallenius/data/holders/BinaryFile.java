package com.wallenius.data.holders;

public class BinaryFile {
    
    private final String contentType;
    private final byte[] content;
    
    public BinaryFile(String contentType, byte[] content) {
        this.contentType = contentType;
        this.content = content;
    }
    
    public String getContentType() {
        return this.contentType;
    }
    
    public byte[] getContent() {
        return this.content;
    }
    
}
