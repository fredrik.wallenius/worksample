package com.wallenius.data;

import com.wallenius.data.holders.BinaryFile;
import com.wallenius.data.holders.Document;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;


public class NaiveDocumentRepository implements DocumentRepository {
    
    @Autowired
    private FileStore fileStore;
    
    private final Map<String, Document> docStore = new HashMap<>();

    @Override
    public void put(Document newDocument, BinaryFile binaryFile) {
        docStore.put(newDocument.getFileName(), newDocument);
        fileStore.save(newDocument.getFileName(), binaryFile);
    }

    @Override
    public Optional<BinaryFile> get(String fileName) {
        return fileStore.get(fileName);
    }

    @Override
    public List<Document> getAll() {
        return new ArrayList<>(docStore.values());
    }

    @Override
    public List<Document> getByCategory(String category) {
        // This is a poor O(n*n) implementation. Sorry.
        List<Document> result = new ArrayList<>();
        docStore.values().forEach(doc -> {
            if (doc.getCategories().contains(category)) {
                result.add(doc);
            }
        });
        return result;
    }
    
}
