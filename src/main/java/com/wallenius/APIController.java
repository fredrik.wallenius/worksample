package com.wallenius;

import com.wallenius.data.holders.BinaryFile;
import com.wallenius.data.holders.Document;
import com.wallenius.data.DocumentRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class APIController {

    @Autowired
    private DocumentRepository docRep;

    @RequestMapping("/documents/{fileName}")
    public ResponseEntity<byte[]> getDocument(@PathVariable String fileName) {

        BinaryFile binaryFile = this.docRep.get(fileName).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline")
                .contentType(MediaType.parseMediaType(binaryFile.getContentType()))
                .body(binaryFile.getContent());
    }

    @PostMapping("/documents")
    public void postDocument(@RequestParam("file") MultipartFile file, @RequestParam String category) throws IOException {

        List<String> categories = this.commaSeparatedStringToList(category);

        Document metaData = new Document(file.getOriginalFilename(), categories);
        BinaryFile binFile = new BinaryFile(file.getContentType(), file.getBytes());

        this.docRep.put(metaData, binFile);
    }

    @RequestMapping("/documents")
    public List<String> listAllOrByCategory(@RequestParam(required = false) String category) {

        if (category == null) {
            return this.metaToArray(this.docRep.getAll());
        } else {
            return this.metaToArray(this.docRep.getByCategory(category));
        }
    }

    private List<String> metaToArray(List<Document> all) {
        List<String> result = new ArrayList<>();
        all.forEach(p -> result.add(p.getFileName()));
        return result;
    }

    private List<String> commaSeparatedStringToList(String categoryString) {
        return Arrays.asList(categoryString.split("\\s*,\\s*"));
    }

}
