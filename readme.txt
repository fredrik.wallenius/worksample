Simple worksample.

To build and run:
Make sure mvn and java is installed. In root folder execute 'mvn spring-boot:run'
API will be available at localhost:8080

API exposes four features in a RESTful manner:

* Upload file
Upload file of any format (below 1MB in size) and specify at least one category. Examples:
$ curl -F 'category=Finance' -F 'file=@contract.txt' http://localhost:8080/documents
$ curl -F 'category=Finance, Contracts' -F 'file=@sample.pdf' http://localhost:8080/documents

* Download file
$ curl http://localhost:8080/documents/contract.txt

* List all files
$ curl http://localhost:8080/documents

* List files by category
$ curl http://localhost:8080/documents?category=Finance


